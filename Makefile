VERSION := 20081224

DESTDIR := /usr/local
ROMSDIR := $(DESTDIR)/share/spectrum-roms
DOCDIR := $(DESTDIR)/share/doc/spectrum-roms

all:
	./make-roms

install: all
	./mkinstalldirs $(ROMSDIR) $(DOCDIR)
	for x in *.rom; do install -m644 $$x $(ROMSDIR)/$$x; done
	install -m644 zx82.htm $(DOCDIR)/

clean:
	-rm -f tc2048.rom

dist: clean
	tar -C .. --exclude debian --exclude .bzr \
		-czvf ../spectrum-roms-$(VERSION).tar.gz \
		spectrum-roms

.PHONY: all install clean dist
